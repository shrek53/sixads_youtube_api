from django.db import models

# Create your models here.
from channels.models import Channel
from tags.models import Tag


class Video(models.Model):
    video_id = models.CharField(max_length=200)
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    tags = models.ManyToManyField(Tag)
    video_description = models.CharField(max_length=400)
    publish_time = models.DateTimeField('date published')
    video_view = models.IntegerField(default=0)

    def __str__(self):
        return self.title
