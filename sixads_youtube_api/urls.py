"""sixads_youtube_api URL Configuration"""
from django.contrib import admin
from django.urls import path, include

from channels.views import ChannelViewSet
from tags.views import TagViewSet
from videos.views import VideoViewSet
from . import views

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'videos', VideoViewSet)
router.register(r'channels', ChannelViewSet)
router.register(r'tags', TagViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
]
