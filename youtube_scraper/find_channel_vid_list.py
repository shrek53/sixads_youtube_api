import os
import argparse
from datetime import datetime, timedelta
from pprint import pprint

import googleapiclient.discovery
from googleapiclient.errors import HttpError

# from .constants import API_KEY

scopes = ["https://www.googleapis.com/auth/youtube.readonly"]


def find_vid_list_of_channel(options):
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    API_KEY = os.environ.get('YOUTUBE_API_KEY')
    youtube = googleapiclient.discovery.build(api_service_name, api_version, developerKey=API_KEY)
    request = youtube.search().list(
        part="id,snippet",
        channelId=options.channel_id,
        maxResults=50,
        type="video",
        order="date",
        # publishedAfter="1970-01-01T00:00:00Z",
        publishedAfter=str((datetime.today() - timedelta(days=20)).replace(hour=0, minute=0, second=0, microsecond=0)
                           .isoformat('T')) + 'Z',
    )

    response = request.execute()
    comma_seperated_ids = ""
    vid_id_list = list(map(lambda item: item['id']['videoId'], response['items']))
    ind = 1
    for vid_id in vid_id_list:
        comma_seperated_ids += vid_id
        if ind < len(vid_id_list):
            comma_seperated_ids += ","
        ind += 1
    f = open(file="vid_id_list.txt", mode="w")
    f.write(comma_seperated_ids)
    f.close()
    pprint(response)
    print(vid_id_list)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--channel-id', help='Channel id', default='UC_x5XG1OV2P6uZZ5FSM9Ttw')
    # parser.add_argument('--max-results', help='Max results', default=25)
    args = parser.parse_args()

    try:
        find_vid_list_of_channel(args)
    except HttpError as e:
        print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
