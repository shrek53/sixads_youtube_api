import json
import os
import argparse
from datetime import datetime
from pprint import pprint

import googleapiclient.discovery
import requests
from googleapiclient.errors import HttpError

api_base_url = 'http://127.0.0.1:8000/'
scopes = ["https://www.googleapis.com/auth/youtube.readonly"]


def insert_channel_info_into_db(channel_info):
    channel_info = channel_info['items'][0] if len(channel_info['items']) >= 1 else None
    if channel_info:
        data = {
            'channel_id': channel_info['id'],
            'channel_title': channel_info['snippet']['title'],
            'channel_description': channel_info['snippet']['description'],
            'published_at': datetime.strptime(channel_info['snippet']['publishedAt'], '%Y-%m-%dT%H:%M:%SZ'),
            'channel_view_count': channel_info['statistics']['viewCount'],
            'channel_subscriber_count': channel_info['statistics']['subscriberCount'],
            'channel_video_count': channel_info['statistics']['videoCount'],
            'country': channel_info['snippet']['country'],
        }
        pprint(data)
        api_response = requests.get(url=api_base_url + f"channels/?channel_id={channel_info['id']}")
        print(api_response.content)
        if len(api_response.content) > 0:
            api_response = requests.put(url=json.loads(api_response.content)[0]['url'], data=data)
        else:
            api_response = requests.post(url=api_base_url + 'channels/', data=data)
        print(api_response.content)


def find_channel_info(options):
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    API_KEY = os.environ.get('YOUTUBE_API_KEY')
    youtube = googleapiclient.discovery.build(api_service_name, api_version, developerKey=API_KEY)
    request = youtube.channels().list(
        part="snippet,contentDetails,statistics",
        id=options.channel_id,
    )

    response = request.execute()
    pprint(response)
    return response


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--channel-id', help='Channel id', default='UC_x5XG1OV2P6uZZ5FSM9Ttw')
    args = parser.parse_args()

    try:
        response = find_channel_info(args)
        insert_channel_info_into_db(response)
    except HttpError as e:
        print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
