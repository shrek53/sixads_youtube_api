import os
import argparse
from datetime import datetime, timedelta
from pprint import pprint

import googleapiclient.discovery
from googleapiclient.errors import HttpError

# from .constants import API_KEY

scopes = ["https://www.googleapis.com/auth/youtube.readonly"]


def prepier_vid_status_data(response):
    vid_data = []
    return vid_data


def find_vid_stats(options):
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    API_KEY = os.environ.get('YOUTUBE_API_KEY')
    youtube = googleapiclient.discovery.build(api_service_name, api_version, developerKey=API_KEY)
    video_id_list = get_video_id_list(options)
    request = youtube.videos().list(
        part="snippet,contentDetails,statistics",
        id=video_id_list
    )
    response = request.execute()
    pprint(response)
    return response


def get_video_id_list(options):
    if options.vid_id_list_dir:
        print("vid ids dir found")
        f = open(file=os.path.dirname(os.path.realpath(__file__))+'/'+options.vid_id_list_dir, mode="r")
        vid_ids = f.read()
        print(vid_ids)
        return vid_ids
    else:
        return options.vid_id_list
    # video_id_list = ["iUcGBTtP4RA,njY12lddTkI"]
    # return video_id_list


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--vid-id-list', help='video id list', default='["Cn5rn6F2dKA"]')
    parser.add_argument('--vid-id-list-dir', help='video id list file name')

    # parser.add_argument('--max-results', help='Max results', default=25)
    args = parser.parse_args()

    try:
        response = find_vid_stats(args)
        prepier_vid_status_data(response)
    except HttpError as e:
        print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
