from django.urls import path, include
from rest_framework import routers

from . import views
from .views import ChannelViewSet

router = routers.DefaultRouter()
router.register(r'channels', ChannelViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
