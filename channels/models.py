from django.db import models


# Create your models here.
class Channel(models.Model):
    channel_id = models.CharField(max_length=200, unique=True)
    channel_title = models.CharField(max_length=200)
    channel_description = models.CharField(max_length=1000)
    published_at = models.DateTimeField('Published date time', null=True)
    channel_view_count = models.IntegerField(default=0)
    channel_subscriber_count = models.IntegerField(default=0)
    channel_video_count = models.IntegerField(default=0)
    country = models.CharField(max_length=50, null=True)
