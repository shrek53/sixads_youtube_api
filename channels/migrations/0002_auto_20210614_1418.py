# Generated by Django 3.2.4 on 2021-06-14 14:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('channels', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='channel',
            old_name='channel_view',
            new_name='channel_subscriber_count',
        ),
        migrations.RemoveField(
            model_name='channel',
            name='publish_time',
        ),
        migrations.RemoveField(
            model_name='channel',
            name='title',
        ),
        migrations.AddField(
            model_name='channel',
            name='channel_video_count',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='channel',
            name='channel_view_count',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='channel',
            name='country',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='channel',
            name='published_at',
            field=models.DateTimeField(null=True, verbose_name='Published date time'),
        ),
    ]
